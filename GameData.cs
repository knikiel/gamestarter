﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStarter
{
    enum Playmode {
        COOP,
        VERSUS,
        BOTH
    }

    struct GameData {
        public string Name { get; set; }
        public string Path { get; set; }
        public Playmode Playmode { get; set; }
        public uint MinPlayers {get; set;}
        public uint MaxPlayers {get; set;}
        public string GameDescription { get; set; }
        public string TrailerLink { get; set; }
    }
}
