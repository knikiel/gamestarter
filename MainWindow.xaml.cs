﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.IO;

namespace GameStarter {
    struct Settings {
        public int NumPlayers { get; set; }
        public Playmode Playmode { get; set; }
    }

    public partial class MainWindow : Window {
        private List<GameData> mAllGameData;
        private Settings mSettings;
        private Uri mNothingSelectedUri;
        private Uri mNoTrailerUri;

        public MainWindow() {
            ReadAllGameData();
            mSettings.NumPlayers = 2;
            mSettings.Playmode = Playmode.BOTH;
            string currentDir = System.IO.Directory.GetCurrentDirectory();
            UriBuilder uriBuilder = new UriBuilder(currentDir + "\\resources\\nothing_selected.html");
            mNothingSelectedUri = uriBuilder.Uri;
            uriBuilder = new UriBuilder(currentDir + "\\resources\\no_trailer.html"); 
            mNoTrailerUri = uriBuilder.Uri;

            InitializeComponent();
            numPlayersSlider.Value = 2;
            webbrowser.Navigate(mNothingSelectedUri);
            this.DataContext = mSettings;
            descriptionPanel.MaxHeight = this.ActualHeight / 3 * 2;
        }

        private void ReadAllGameData() {
            mAllGameData = new List<GameData>();
            int numLinesRead = 0;
            using(var reader = new StreamReader("gamedata.cfg")) {
                while (!reader.EndOfStream)
                {
                    numLinesRead += 1;
                    var rawGameData = reader.ReadLine().Trim();
                    if (rawGameData[0] == '#')
                    {
                        // Kommentar
                        continue;
                    }
                    var splitGameData = rawGameData.Split(';');
                    if (splitGameData.Length != 7)
                    {
                        MessageBox.Show("\"" + rawGameData + "\" muss entweder 7 Felder enthalten und mit einem '#' beginnen.",
                            "Zeile " + numLinesRead + " ist nicht korrekt formatiert!");
                        continue;
                    }
                    string name = splitGameData[0];

                    Playmode mode = Playmode.BOTH;
                    switch (splitGameData[1])
                    {
                        case "C":
                            {
                                mode = Playmode.COOP;
                                break;
                            }
                        case "V":
                            {
                                mode = Playmode.VERSUS;
                                break;
                            }
                        case "B":
                        case "CV":
                        case "VC":
                            {
                                mode = Playmode.BOTH;
                                break;
                            }
                        default:
                            MessageBox.Show("\"" + splitGameData + "\" muss \"C\" (COOP), \"V\" (Gegeneinander/Team)"
                                + " oder \"B\", \"CV\", \"VC\" (beide Varianten mögliche)  sein.",
                                "Zeile " + numLinesRead + " ist nicht korrekt formatiert!");
                            continue;
                    }

                    uint minPlayers, maxPlayers;
                    try
                    {
                        minPlayers = UInt32.Parse(splitGameData[2]);
                        maxPlayers = UInt32.Parse(splitGameData[3]);
                    }
                    catch (FormatException e)
                    {
                        MessageBox.Show("\"" + splitGameData[2] + "\" und/oder \"" + splitGameData[3] + "\" keine mögliche Spielerzahl.",
                            "Zeile " + numLinesRead + " ist nicht korrekt formatiert!");
                        continue;
                    }

                    string link = splitGameData[4];
                    Uri uri;
                    if (link != "" && !Uri.TryCreate(link, UriKind.Absolute, out uri))
                    {
                        MessageBox.Show("\"" + link + "\" ist keine korrekt formatierte URI. Spiel wird dennoch akzeptiert.",
                                "Zeile " + numLinesRead + " ist nicht korrekt formatiert!");
                        link = "";
                    }

                    string description = splitGameData[5];
                    string path = splitGameData[6];
                    mAllGameData.Add(new GameData { Name = name, MinPlayers = minPlayers, MaxPlayers = maxPlayers,
                        Path = path, Playmode = mode, GameDescription = description, TrailerLink = link});
                }
            }
        }

        private void RefreshRelevantGameData() {
            var relevantGameData = new List<GameData>();
            foreach (var gameData in mAllGameData) {
                if (mSettings.Playmode == Playmode.BOTH
                    || (mSettings.Playmode == Playmode.COOP && gameData.Playmode != Playmode.VERSUS)
                    || (mSettings.Playmode == Playmode.VERSUS && gameData.Playmode != Playmode.COOP))
                {
                    if (gameData.MinPlayers <= mSettings.NumPlayers && mSettings.NumPlayers <= gameData.MaxPlayers) {
                        relevantGameData.Add(gameData);
                    }
                }
            }
            gamesView.ItemsSource = relevantGameData;
            if (relevantGameData.Count == 0) {
                StartRandomButton.IsEnabled = false;
            }
            else {
                StartRandomButton.IsEnabled = true;
            }
        }

        private bool startGame(string path)
        {
            // TODO: Fehler besser abfangen
            bool existsFile = System.IO.File.Exists(path);
            if (!existsFile)
            {
                return false;
            }
            var process = System.Diagnostics.Process.Start(path);
            //TODO: was ist, wenn nicht ausführbar?
            return true;
        }

        // TODO: soll nur geklickt werden können, wenn überhaupt ein Spiel zur verfügung steht
        private void StartRandomButtonClicked(object sender, RoutedEventArgs e) {
            Debug.Assert(gamesView.Items.Count > 0);
            var rng = new Random();
            int idxRandom = rng.Next(0, gamesView.Items.Count);
            GameData selectedGameData = (GameData) gamesView.Items[idxRandom];
            bool gameStarted = false;
            while (!gameStarted) {
                gameStarted = startGame(selectedGameData.Path);
            }
        }

        // TODO: soll nur geklickt werden können, wenn das ausgewählte Spiel überhaupt einen Pfad angegeben hat.
        private void StartChosenButtonClicked(object sender, RoutedEventArgs e) {
            Debug.Assert(gamesView.SelectedIndex != -1);
            int idxSelected = gamesView.SelectedIndex;
            GameData selectedGameData = (GameData) gamesView.Items[idxSelected];
            startGame(selectedGameData.Path);
        }

        private void ModeRadioClicked(object sender, RoutedEventArgs e) {
            if (sender == vsRadio) {
                mSettings.Playmode = Playmode.VERSUS;
            }
            else if (sender == coopRadio) {
                mSettings.Playmode = Playmode.COOP;
            }
            else if (sender == bothRadio) {
                mSettings.Playmode = Playmode.BOTH;
            }
            else {
                Debug.Assert(false);
            }
            RefreshRelevantGameData();
        }

        private void numPlayersSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
            mSettings.NumPlayers = (int) ((Slider) sender).Value;
            RefreshRelevantGameData();
        }

        private void setWebbrowserVisible(bool visible) {
            if (visible) {
                webbrowser.MaxHeight = this.ActualHeight / 3 * 2;
                webbrowser.Visibility = Visibility.Visible;
            }
            else {
                webbrowser.Visibility = Visibility.Hidden;
                webbrowser.Navigate(mNoTrailerUri);
                webbrowser.MaxHeight = 0;
            }
        }

        private void setDescriptionVisible(bool visible) {
            if (visible) {
                gameDescription.MaxHeight = this.ActualHeight / 3 * 2;
                gameDescription.Visibility = Visibility.Visible;
            }
            else {
                gameDescription.Visibility = Visibility.Hidden;
                gameDescription.MaxHeight = 0;
            }
        }

        private void gamesViewSelectedRowChanged(object sender, SelectedCellsChangedEventArgs evt) {
            int idxSelected = gamesView.SelectedIndex;
            if (idxSelected < 0) {
                StartChosenButton.IsEnabled = false;
                setWebbrowserVisible(true);
                setDescriptionVisible(false);
                webbrowser.Navigate(mNothingSelectedUri);
                return;
            }

            StartRandomButton.IsEnabled = true;
            StartChosenButton.IsEnabled = true;
            GameData selectedGameData = (GameData) gamesView.Items[idxSelected];
            if (selectedGameData.TrailerLink != "") {
                setWebbrowserVisible(true);
                webbrowser.Navigate(selectedGameData.TrailerLink);
            }
            else {
                setWebbrowserVisible(false);
            }
            if (selectedGameData.GameDescription != "") {
                setDescriptionVisible(true);
                gameDescriptionLabel.Text = selectedGameData.GameDescription;
            }
            else {
                if (selectedGameData.TrailerLink == "") {
                    gameDescriptionLabel.Text = "Keine Beschreibung vorhanden.";
                    setDescriptionVisible(true);
                }
                else {
                    setDescriptionVisible(false);
                }
            }
        }
    }
}
